# main-server

Дипломный проект по визуализации геоданных с помощью Cesium.js.

Что тут есть:

- Бекенд на Django
- Фронтенд на Cesium

# Сборка

В системе должны быть доступны следующие инструменты:

- [pipenv](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)
- [node](https://nodejs.org/en/) (удобно ставить с помощью [nvm](https://github.com/creationix/nvm))



Установить зависимости для Python
```
pipenv install
```

# Полезные ссылки
https://pipenv.readthedocs.io/en/latest/basics/