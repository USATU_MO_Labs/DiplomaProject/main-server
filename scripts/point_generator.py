# latitude:
#   0 at equator
#   90 at the north pole
#   -90 at the south pole

# longtitude:
#   0 at Greenwich
#   negative to the east, up to -180
#   positive to the west, up to 180

# pairs (latitude, longtitude)


def generate_points(**kwargs):
    # latitude
    lat_min = kwargs.get("lat_min", -90)
    lat_max = kwargs.get("lat_max", 90)

    # longtitude
    long_min = kwargs.get("long_min", -180)
    long_max = kwargs.get("long_max", 180)

    # elevation
    el_min = kwargs.get("el_min", 0)
    el_max = kwargs.get("el_max", 100)

    # point count
    n = kwargs.get("n", 100)

    def randfloat(a, b):
        """
        Generates a random float between a and b.
        """
        from random import random
        return a + (b - a) * random()

    points = []

    for _ in range(n):
        point = {
            "latitude": randfloat(lat_min, lat_max),
            "longtitude": randfloat(long_min, long_max),
            "elevation": randfloat(el_min, el_max)
        }

        points.append(point)

    return points


if __name__ == "__main__":
    import argparse
    import json

    parser = argparse.ArgumentParser(
        description="Generates some lat-long-elevation points randomly with given parameters")
    parser.add_argument(
        "file", help="Where to dump json with points", type=str)
    parser.add_argument(
        "n", help="How many points should be generated", type=int)
    parser.add_argument("--lat_min", help="Minimum latitude",
                        type=float, default=-90)
    parser.add_argument("--lat_max", help="Maximum latitude",
                        type=float, default=90)
    parser.add_argument("--long_min", help="Minimum longtitude",
                        type=float, default=-180)
    parser.add_argument("--long_max", help="Maximum longtitud",
                        type=float, default=180)
    parser.add_argument("--el_min", help="Minimum elevation",
                        type=float, default=0)
    parser.add_argument("--el_max", help="Maximum elevation",
                        type=float, default=100)

    args = parser.parse_args()

    # n=500, el_min=45.78, el_max=56.43
    points = generate_points(
        n=args.n,
        lat_min=args.lat_min,
        lat_max=args.lat_max,
        long_min=args.long_min,
        long_max=args.long_max,
        el_min=args.el_min,
        el_max=args.el_max
    )

    with open(args.file, "w") as f:
        json.dump(points, f)
